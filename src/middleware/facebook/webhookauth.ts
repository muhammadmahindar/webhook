import { Request, Response, NextFunction } from 'express';
import crypto from 'crypto';
function getSignature(buf: Buffer) {
    var token = process.env.APP_SECRET || 'token';
    var hmac = crypto.createHmac("sha1", token);
    hmac.update(buf.toString('utf-8'), "utf8");
    return "sha1=" + hmac.digest("hex");
}

export const validateXHubSignature = (req: Request, res: Response, buf: Buffer) => {
    var expected = req.headers['x-hub-signature'];
    var calculated = getSignature(buf);
    if (expected !== calculated) {
        res.sendStatus(401);
    }
}