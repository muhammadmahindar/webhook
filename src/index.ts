import dotenv from 'dotenv';
dotenv.config();
import bodyParser from 'body-parser';
import express from 'express';
import * as facebookController from './controller/facebook';
import * as fbWebhookAuth from './middleware/facebook/webhookauth';
var app = express();

app.set('port', (process.env.APP_PORT || 3000));
app.listen(app.get('port'));
app.use(bodyParser.json({ verify: fbWebhookAuth.validateXHubSignature }));
app.get('/webhook', facebookController.getWebhook);

app.post('/webhook', facebookController.postWebhook);
export default app;