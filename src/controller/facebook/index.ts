import crypto from 'crypto';
import { Response, Request } from "express";
import rabbitMq from "../../core/QueueManager/index";

export const getWebhook = (req: Request, res: Response) => {


    var token = process.env.APP_TOKEN || 'token';
    if (
        req.query['hub.mode'] == 'subscribe' &&
        req.query['hub.verify_token'] == token
    ) {
        res.send(req.query['hub.challenge']);
    } else {
        res.sendStatus(400);
    }
}

export const postWebhook = (req: Request, res: Response) => {
    const queue = new rabbitMq(JSON.stringify(req.body));
    queue.logWebhook();
    res.sendStatus(200);
}