import amqp, { Connection, Channel, } from 'amqplib';

class Queue {
    message: string;
    constructor(message: string) {
        this.message = message;
    }
    logWebhook() {
        var queue = process.env.QUEUE_NAME || 'default';
        var url = "amqp://" + process.env.QUEUE_USERNAME + ":" + process.env.QUEUE_PASSWORD + "@" + process.env.QUEUE_HOST;
        amqp.connect(url).then((connection: Connection) => {
            connection.createChannel().then((channel: Channel) => {
                channel.assertQueue(queue, {
                    durable: true
                });
                channel.sendToQueue(queue, Buffer.from(this.message));
            });
        })

    }
}


export default Queue;